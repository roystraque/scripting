import nmap

def scanner_ips(subnet, arguments):
    nm = nmap.PortScanner()
    nm.scan(hosts=subnet, arguments=arguments)

    ip_service_versions = {}
    for host in nm.all_hosts():
        services = []
        for proto in nm[host].all_protocols():
            lport = nm[host][proto].keys()
            for port in lport:
                service_info = nm[host][proto][port]
                service = f"{service_info['name']} ({service_info['product']} {service_info['version']})"
                services.append(service)
        ip_service_versions[host] = services

    return ip_service_versions

def menu():
    print("1. Scan TCP/IP")
    print("2. Scan UDP")
    print("3. Scan OS (version du noyau)")
    print("4. Scan OS (version des services)")
    print("5. Quitter")

if __name__ == "__main__":
    subnet = input("Entrez le sous-réseau à scanner (par exemple, 192.168.1.0/24): ")

    while True:
        menu()
        choix = input("Choisissez une option (1-5): ")

        if choix == "1":
            ip_service_versions = scanner_ips(subnet, "-p 1-1000")  # Scan TCP/IP
        elif choix == "2":
            ip_service_versions = scanner_ips(subnet, "-sU")  # Scan UDP
        elif choix == "3":
            ip_service_versions = scanner_ips(subnet, "-O -sS")  # Scan OS (version du noyau)
        elif choix == "4":
            ip_service_versions = scanner_ips(subnet, "-sV")  # Scan OS (version des services)
        elif choix == "5":
            print("Au revoir!")
            break
        else:
            print("Option invalide. Veuillez choisir une option valide.")

            print(nm.all_hosts())

        print("Résultats du scan:")
        for ip, services in ip_service_versions.items():
            print(f"\nAdresse IP: {ip}")
            print("Services:")
            for service in services:
                print(f"- {service}")
